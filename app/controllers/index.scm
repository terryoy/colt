;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2017,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;; Controller index definition of colt
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller index) ; DO NOT REMOVE THIS LINE!!!

(use-modules (app models posts)
             (colt config))

(http-status
 404
 (lambda ()
   (view-render "404" (the-environment))))

;; NOTE: We may use more efficient half-static cache, which saves
;;       dynamic content to a static file, and use send-file for
;;       sending it. This reduces half time for each request, but
;;       cache missed. So we just use dynamic cache here, the first
;;       time fetching is half time slower but then the cache works
;;       for client.
(get "/"
  (lambda (rc)
    (redirect-to rc "/index")))

(index-define
 ""
 (lambda (rc)
   (or (try-to-get-page-from-cache rc)
       (let ((blog-name (colt-conf-get 'blog-name))
             (post-content (tpl->html
                            (generate-editable-index-content "simplified"))))
         (cache-this-page rc (view-render "index" (the-environment)))))))

(get "/robots\\.txt"
  (lambda ()
    (response-emit
     "User-agent: Googlebot
Allow: /index
Allow: /archives
Allow: /pub
Allow: /feed
Allow: /about
Disallow: /dashboard
Disallow:: /article

User-agent: *
Allow: /archives
Allow; /feed
Disallow:: /dashboard
Disallow:: /article
Disallow: /pub
Allow: /about
"
     #:headers '((content-type . (text/plain (charset . "utf-8")))))))
