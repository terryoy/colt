;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2017,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

;; Controller articles definition of colt
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller articles) ; DO NOT REMOVE THIS LINE!!!

(use-modules (artanis artanis)
             (app models posts)
             (artanis irregex)
             (web uri)
             (colt config))

;; TODO: use cache
(get "/article/oid/:oid"
  #:cache #t
  (lambda (rc)
    (let ((article-content (tpl->html (get-one-article-by-oid (params rc "oid"))))
          (account (colt-conf-get 'disqus)))
      (view-render "article" (the-environment)))))

;; TODO: use cache
(get "/article/preview/oid/:oid"
  #:cache #t
  (lambda (rc)
    (let ((article-content (tpl->html (get-one-article-by-oid (params rc "oid")))))
      (view-render "preview" (the-environment)))))

(get "/article/preview/:name"
  #:cache #t
  (lambda (rc)
    (let ((article-content (get-one-article (params rc "name"))))
      (view-render "preview" (the-environment)))))

(get "/article/:name"
  #:cache #t
  (lambda (rc)
    (let ((article-content (get-one-article (params rc "name")))
          (account (colt-conf-get 'disqus)))
      (view-render "article" (the-environment)))))

(define *archive-re* (string->irregex "/archives/(.*)"))
(define (path->url path)
  (let ((m (irregex-match *archive-re* path)))
    (if m
        (string-downcase (irregex-match-substring m 1))
        (throw 'artanis-err 404 path->url
               "Invalid path `~a'" path))))
(get "/archives/.*"
  #:cache #t
  (lambda (rc)
    (let ((article-content (get-one-article (path->url (rc-path rc))))
          (account (colt-conf-get 'disqus)))
      (view-render "article" (the-environment)))))
