;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2017,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

;; Model posts definition of colt
;; This file is generated automatically by GNU Artanis.
(define-module (app models posts)
  #:use-module (colt git)
  #:use-module (colt config)
  #:use-module (colt utils)
  #:use-module (artanis artanis)
  #:use-module (artanis env)
  #:use-module (artanis utils)
  #:use-module (artanis irregex)
  #:use-module (srfi srfi-1)
  #:use-module ((rnrs) #:select (get-string-n))
  #:export (update-index-posts-cache
            get-one-article
            get-all-posts
            get-posts-by-tag
            count-tags
            get-ordered-tags-list
            get-intro-page
            get-the-latest-post
            get-the-latest-post-update-time
            get-article-content-by-name
            get-intro-content
            generate-editable-index-content
            generate-editable-intro-content
            get-one-article-by-oid
            post-title
            post-tags
            post-timestamp))

(define (post-title p)
  (meta-data-title (post-meta-data p)))

(define (post-timestamp p)
  (meta-data-timestamp (post-meta-data p)))

(define (post-tags p)
  (meta-data-tags (post-meta-data p)))

(define (post-status p)
  (meta-data-status (post-meta-data p)))

(define (post-comentable? p)
  (let ((check (meta-data-comment-status (post-meta-data p))))
    (string=? check open)))

(define *all-post-objs* '())
(define *intro* '())
(define *all-tags* (make-hash-table))

(define* (get-all-post-objs #:key (latest-top? #t) (refresh? #f))
  (define (sort-by-time objs)
    (sort objs (lambda (x y)
                 (>= (string->number (post-timestamp (get-post x)))
                     (string->number (post-timestamp (get-post y)))))))
  (define return (if latest-top? identity reverse))
  (define (filter-me objs)
    (partition (lambda (o)
                 (string=? "_____colt_____Intro" (post-title (get-post o))))
               objs))
  (when refresh? (set! *all-post-objs* '()))
  (cond
   ((not (null? *all-post-objs*))
    (return *all-post-objs*))
   (else
    (let ((objs (sort-by-time (git/get-post-objs))))
      (call-with-values
          (lambda () (filter-me objs))
        (lambda (intro all)
          (set! *all-post-objs* all)
          (set! *intro* (map get-post intro))

          ;; init tags
          (for-each
           (lambda (o)
             (for-each
              (lambda (t)
                (let ((pl (hash-ref *all-tags* t '())))
                  (hash-set! *all-tags* t (cons (get-post o) pl))))
              (tags-string->list (post-tags (get-post o)))))
           all)))
      (return *all-post-objs*)))))

(define* (get-all-posts #:key (latest-top? #t) (refresh? #f))
  (map get-post (get-all-post-objs #:latest-top? latest-top? #:refresh? refresh?)))

(define (get-posts-by-tag tag)
  (hash-ref *all-tags* (uri-decode tag) '()))

(define (count-tags)
  (hash-count (const #t) *all-tags*))

(define (get-ordered-tags-list)
  (sort (hash-map->list cons *all-tags*)
        (lambda (x y) (<= (length x) (length y)))))

(define (get-intro-page)
  (when (null? *intro*)
    ;; No intro, try to refresh
    (get-all-posts #:refresh? #t))
  (cond
   ((null? *intro*)
    ;; Still empty means no intro page
    (post-intro-page)
    (get-all-posts #:refresh? #t)
    (get-intro-page))
   (else *intro*)))

(define (get-the-latest-post)
  (car (get-all-posts #:latest-top? #t)))

(define (get-the-latest-post-update-time)
  (post-timestamp (get-the-latest-post)))

(define (update-index-posts-cache)
  (clear-content-cache "index")
  (clear-content-cache "atom/feed")
  (get-all-posts #:refresh? #t))

(define (get-posts-from-n-to-m n m posts-list)
  (if (null? posts-list)
      posts-list
      (list-head (list-tail posts-list n) m)))

(define (get-post-abstract posts-list)
  (define (->abstract p)
    (call-with-input-string
        (post-content p)
      (lambda (port)
        (get-string-n port (colt-conf-get 'abstract-size)))))
  (map ->abstract posts-list))

(define* (gen-one-post post mode #:optional (need-abstract? #f))
  (define-syntax-rule (->url url-name)
    (format #f "/archives/~a" (uri-decode url-name)))
  (define-syntax-rule (->title title url)
    `(div (@ (class "blog-post-title"))
          (a (@ (href ,(->url (post-url-name post)))) ,title)))
  (define-syntax-rule (->author author)
    `(a (@ (href ,(colt-conf-get 'github-url))
           (id "author"))
        ,author))
  (define-syntax-rule (->timestamp timestamp)
    (timestamp->readable-date (string->number timestamp)))
  (define-syntax-rule (->tags tags)
    (let lp ((t tags) (ret '("")))
      (cond
       ((null? t) ret)
       (else
        (lp (cdr t)
            (list (if (null? (cdr t)) "" " ")
                  `(a (@ (href ,(format #f "/tags/~a" (car t))) (class "blog-tag")) ,(car t))
                  ret))))))
  (define-syntax-rule (->content content url-name)
    (cond
     ((string=? mode "simplified")
      `(div (@ (class "blog-post-content"))
            ,(trim-content content)
            (div (a (@ (href ,(->url (post-url-name post)))) "More..."))))
     ((string=? mode "edit")
      `(div (@ (class "blog-post-content"))
            ,content))))
  (let ((title (post-title post))
        (timestamp (post-timestamp post))
        (author (colt-conf-get 'blog-author))
        (url-name (post-url-name post))
        (tags (tags-string->list (post-tags post)))
        (content (post-content post))
        (status (post-status post))
        (oid (post-oid post)))
    `(div (@ (class "blog-post"))
          ,(->title title (->url url-name))
          (div (@ (class "blog-post-meta"))
               "Posted by " ,(->author author)
               ,(->timestamp timestamp))
          (div (@ (class "blog-tags")) ,(->tags tags))
          (div (@ (id "blog-status") (class ,(format #f "blog-status blog-status-~a" status))) ,status)
          ,(cond
            ((string=? mode "edit")
             `(div (@ (class "blog-edit-buttons"))
                   (button (@ (class "btn blue darken-2 edit-buttons")
                              (onclick "edit_post(this)"))
                           "Edit")
                   (button (@ (id ,(string-append "delete-button-" oid))
                              (class "btn red accent-2 edit-buttons")
                              (onclick "delete_post(this)"))
                           "Delete"))))
          ,(->content content url-name)
          (p (@ (class "blog-oid")) ,oid))))

(define* (gen-one-article post #:optional (need-abstract? #f))
  (define-syntax-rule (->title title url)
    `(div (@ (class "blog-post-title"))
          (h4 ,(if (string=? title "_____colt_____Intro") "About me" title))))
  (define-syntax-rule (->author author)
    `(a (@ (href ,(colt-conf-get 'github-url))
           (id "author"))
        ,author))
  (define-syntax-rule (->timestamp timestamp)
    (timestamp->readable-date (string->number timestamp)))
  (define-syntax-rule (->tags tags)
    (let lp ((t tags) (ret '("")))
      (cond
       ((null? t) (cons "" ret))
       (else
        (lp (cdr t)
            (list (if (null? (cdr t)) "" " ")
                  `(a (@ (href ,(format #f "/tags/~a" (car t))) (class "blog-tag")) ,(car t))
                  ret))))))
  (define-syntax-rule (->content content url-name)
    `(div (@ (class "blog-post-content"))
          ,content))
  (let ((title (post-title post))
        (timestamp (post-timestamp post))
        (author (colt-conf-get 'blog-author))
        (url-name (post-url-name post))
        (tags (tags-string->list (post-tags post)))
        (content (post-content post)))
    `(div (@ (class "blog-post"))
          ,(->title title (uri-decode url-name))
          (div (@ (class "blog-post-meta"))
               "Posted by " ,(->author author)
               ,(->timestamp timestamp))
          (div (@ (class "blog-tags")) ,(->tags tags))
          ,(->content content url-name))))

(define (generate-pages-content mode posts)
  (fold-right
   (lambda (post prev)
     (cond
      ((string=? (post-status post) "publish")
       (cons (gen-one-post post mode 'abstracted) prev))
      ((and (string=? mode "edit")
            (not (string=? (post-status post) "deleted")))
       (cons (gen-one-post post mode 'abstracted) prev))
      (else prev)))
   '()
   posts))

(define (generate-editable-index-content mode)
  (generate-pages-content mode (get-all-posts #:latest-top? #t)))

(define (generate-editable-intro-content mode)
  (generate-pages-content mode (get-intro-page)))

(define (get-one-article-by-oid oid)
  (let ((post (get-post-by-oid oid (get-all-post-objs))))
    (when (not post)
      (throw 'artanis-err 404 get-one-article-by-oid
             "Invalid oid (~a)!" oid))
    (gen-one-article post)))

(define (get-one-article url-name)
  (tpl->html
   (let ((post (get-post-by-url-name url-name (get-all-posts))))
     (when (not post)
       (throw 'artanis-err 404 get-one-article
              "Invalid url-name `~a'!" url-name))
     `(div (@ (class "article"))
           ,(gen-one-article post)))))

(define (get-article-content-by-name url-name)
  (let ((post (get-post-by-url-name url-name (get-all-posts))))
    (post-content post)))

(define (get-intro-content)
  (let ((intro (get-intro-page)))
    (post-content (car intro))))
