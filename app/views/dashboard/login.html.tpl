<!DOCTYPE html>
<html>
    <head>
        <title><%= (current-appname) %></title>
        <@icon favicon.png %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="language" content="english">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="referrer" content="strict-origin-when-cross-origin">
        <@css materialize.min.css %>
        <@css blog.css %>
        <@css custom-blue-theme.css %>
        <@js jquery-3.3.1.min.js %>
        <@js materialize.min.js %>
        <script>
         $("#reset").on("click", function() {
             $('label').removeClass('active');
         });
        </script>
    </head>

    <body style="background-image:url('/pub/img/bg.jpg');">
        <div class="valign-wrapper row login-box center-align">
            <div class="col card hoverable s10 pull-s1 m6 pull-m3 l4 pull-l4">
                <form action="/auth" method="post">
                    <div class="card-content">
                        <span class="card-title blue-text">Colt Login</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <label for="username">Username</label>
                                <input type="text" class="validate" name="username" id="username" />
                            </div>
                            <div class="input-field col s12">
                                <label for="password">Password </label>
                                <input type="password" class="validate" name="password" id="password" />
                            </div>
                        </div>
                    </div>
                    <%= (if (equal? failed "true")
                            (tpl->html `(div (@ (class "red-text"))
                                             "Invalid username or passwd!"))
                            "")
                    %>
                    <div class="card-action right-align">
                        <button type="reset" class="waves-effect waves-blue blue-text btn-flat">reset</button>
                        <button type="submit" class="waves-effect waves-blue blue-text btn-flat">login</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
    <@include foot.html.tpl %>
</html>
