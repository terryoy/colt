<!DOCTYPE html>
<html>
    <head>
        <title><%= (current-appname) %></title>
        <@icon favicon.ico %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="language" content="english">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'>
        <meta name="referrer" content="strict-origin-when-cross-origin">
        <link rel="preload" as="font" href="/pub/css/fonts/OpenSans-Regular.ttf">
        <@css materialize.min.css %>
        <@js materialize.min.js %>
        <@css blog.css %>
        <@css custom-blue-theme.css %>
        <@js dashboard.js %>
    </head>

    <body>
        <div class="row">
            <form id="setting_form">
                <div class="row">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder=<%= title %> name="title" type="text" class="validate">
                            <label for="title">Your blog title</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder=<%= author %> name="author" type="text" class="validate">
                            <label for="title">Your name</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder=<%= blog-url %> name="blog-url" type="text" class="validate">
                            <label for="title">Your blog url</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder=<%= disqus %> name="disqus" type="text" class="validate">
                            <label for="title">Your disqus account</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder=<%= disqus-pubkey %> name="disqus-pubkey" type="text" class="validate">
                            <label for="title">Your disqus public token</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input id="old-passwd" name="old-passwd" type="password" class="validate">
                            <label for="old-passwd">Old password:</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="new-passwd" name="new-passwd" type="password" class="validate">
                            <label for="new-passwd">New password</label>
                        </div>
                    </div>

                    <div class="card-action left-align">
                        <button type="button" onclick="reset_settings()" class="waves-effect waves-blue blue-text btn-flat">reset</button>
                        <button type="button" onclick="save_settings()" class="waves-effect waves-blue blue-text btn-flat">save</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
