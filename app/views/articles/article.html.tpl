<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
        <@css article.css %>
        <@css monokai-sublime.min.css %>
        <@css katex.min.css %>
        <@js katex.min.js %>
    </head>

    <body>
        <@include common_nav.html.tpl %>
        <div class="main-content container">
            <%= article-content %>
            <@include comment.html.tpl %>
        </div>
    </body>

    <@include foot.html.tpl %>
</html>
