;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2017,2018,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

(define-module (colt git)
  #:use-module (artanis env)
  #:use-module (artanis utils)
  #:use-module (artanis irregex)
  #:use-module (colt cmd)
  #:use-module (colt config)
  #:use-module (colt utils)
  #:use-module ((rnrs) #:select (define-record-type get-string-all))
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1)
  #:export (current-blog-repo
            git-ls-tree
            git/get-post-objs
            git/get-meta-data
            get-post-by-url-name
            get-object-by-oid
            get-object-by-name
            get-post-by-oid
            get-post

            post-meta-data
            post-content
            post-comment
            post-url-name
            post-oid

            meta-data-timestamp
            meta-data-tags
            meta-data-status
            meta-data-title
            meta-data-name
            meta-data-comment-status

            comment-timestamp
            comment-author
            comment-email
            comment-site
            comment-content

            enter-blog-repo
            ensure-blog-repo
            git-post-article
            git-edit-article
            git-remove-article
            git-modify-article
            tags-string->list
            post-intro-page))

;; NOTE: Must be absolute path
(define (current-blog-repo)
  (format #f "~a/~a" (current-toplevel) (colt-conf-get 'blog-repo)))

(define-record-type git-object
  (fields mode type oid file))

(define-record-type post
  (fields meta-data content comment url-name oid))

(define-record-type meta-data
  (fields
   (mutable timestamp)
   (mutable tags)
   (mutable status)
   (mutable title)
   name
   comment_status))

(define-record-type comment
  (fields timestamp author email site content))

(define-syntax-rule (-> l)
  (string-trim-both l))

(define-syntax-rule (rdline port)
  (-> (read-line port)))

(define *meta-data-res*
  `((timestamp . ,(string->irregex "^timestamp:(.*)"))
    (tags      . ,(string->irregex "^tags:(.*)"))
    (status    . ,(string->irregex "^status:(.*)"))
    (title     . ,(string->irregex "^title:(.*)"))
    (name      . ,(string->irregex "^name:(.*)"))
    (comment_status . ,(string->irregex "^comment_status:(.*)"))))

(define *comment-res*
  `((timestamp . ,(string->irregex "^timestamp:(.*)"))
    (author    . ,(string->irregex "^author:(.*)"))
    (email     . ,(string->irregex "^author_email:(.*)"))
    (site      . ,(string->irregex "^author_url:(.*)"))
    (content   . ,(string->irregex "^content:(.*)"))))

(define (get-re re-table key)
  (let ((re (assoc-ref re-table key)))
    (if re
        re
        (throw 'artanis-err 500 get-re
               "Invalid meta-data regex: ~a~%" re))))

(define (get-value re-table key str)
  (let* ((re (get-re re-table key))
         (m (irregex-search re str)))
    (if m
        (irregex-match-substring m 1)
        (throw 'artanis-err 500 get-value
               "~a has parsed an invalid line ~a~%" key str))))

(define (get-meta-data-value key port)
  (-> (get-value *meta-data-res* key (read-line port))))

(define (get-comment-value key port)
  (-> (get-value *comment-res* key (read-line port))))

(define (new-comment oid)
  (define (get-comment-content port)
    (read-line port)
    (get-string-all port))
  (call-with-input-string
      (raw-cmd git show ,oid)
    (lambda (port)
      (let* ((timestamp (get-comment-value 'timestamp port))
             (author (get-comment-value 'author port))
             (email (get-comment-value 'email port))
             (site (get-comment-value 'site port))
             (content (get-comment-content port)))
        (make-comment timestamp
                      author
                      email
                      site
                      content)))))

(define (get-object-by-oid oid obj-list)
  (any (lambda (o) (and (string=? oid (git-object-oid o)) o)) obj-list))

(define (get-object-by-name name obj-list)
  (any (lambda (o) (and (string=? name (git-object-file o)) o)) obj-list))

(define (get-post-by-url-name url-name post-list)
  (any (lambda (p) (and (string=? url-name (uri-decode (post-url-name p))) p)) post-list))

(define (get-post-by-oid oid obj-list)
  (any (lambda (o) (and (string=? oid (git-object-oid o)) (get-post o))) obj-list))

(define* (git-ls-tree #:key (branch "master"))
  (cmd-result-map
   (lambda (o)
     (match o
       ((mode type oid file)
        (make-git-object mode type oid file))
       (else (throw 'artanis-err 500 git-ls-tree
                    "BUG: Invalid git object format ~a" o))))
   (cmd git ls-tree ,branch)))

;; string -> file-id
(define* (git-hash-object content #:optional (type 'blob))
  (let ((tmp (mkstemp! (string-copy "/tmp/colt-XXXXXX"))))
    (display content tmp)
    (force-output tmp)
    (let ((cmdline (format #f "git hash-object -t ~a -w ~a"
                           type (port-filename tmp))))
      (one-line-result ,cmdline))))

;; file-id -> file-path -> oct-int -> void
(define (git-update-index op file-id file-path mode)
  (let ((info (format #f "~a,~a,~a" mode file-id file-path)))
    (one-line-result git update-index ,op --cacheinfo ,info)))

(define (git-op-generator op)
  (lambda* (file-id file-path #:optional (type 'file))
    (let ((mode (case type
                  ((file) "100644")
                  ((dir) "040000")
                  (else (throw 'artanis-err 500 git-op-generator
                               "Invalid type `~a'" type)))))
      (git-update-index op file-id file-path mode))))

(define git-add (git-op-generator "--add"))
(define git-rm (git-op-generator "--remove"))
(define git-modify (git-op-generator "--add --replace"))

;; void -> tree-id
(define* (git-write-tree #:key prefix)
  (let ((opts (if prefix (format #f "--prefix=~a/" prefix) "")))
    (one-line-result git write-tree ,opts)))

;; tree-id -> string -> commit-id
(define* (git-commit-tree tree-id message #:key (parent 'HEAD))
  (one-line-result git commit-tree
                   ,(if parent (format #f "-p ~a" parent) "")
                   -m ,(object->string message)
                   ,tree-id))

(define* (git-update-ref commit-id #:optional (tree "refs/heads/master"))
  (one-line-result git update-ref ,tree ,commit-id))

(define (gen-meta-content meta)
  (call-with-output-string
    (lambda (port)
      (for-each
       (lambda (m)
         (format port "~a: ~a~%" (car m) (cdr m)))
       meta))))

;; 1. git hash-object -w --stdin < file-content -> file-id
;; 2. echo -n "100644 blob id\tfile-path\n" | git mktree -> tid
;; 3. git commit-tree -m "message" tree-id  ->  commit-id
;; 4. git update-ref refs/heads/master commit-id
(define* (git-post-article article-name content meta #:key (init? #f))
  (define (filter-existing-file)
    (string-join
     (filter-map
      (lambda (s)
        (if (irregex-search article-name s) #f s))
      (string-split (one-line-result git ls-tree master) #\nl))
     "\n"))
  (define (get-current-file-info)
    (if init?
        ""
        (filter-existing-file)))
  (define (create-post-dir cid mid)
    (let* ((cur (get-current-file-info))
           (c (format #f "100644 blob ~a~/content" cid))
           (m (format #f "100644 blob ~a~/metadata" mid))
           (tid (one-line-result
                 ,(format #f "echo -n \"~a~%~a~%\" | git mktree" c m))))
      (values
       tid
       (one-line-result
        ,(format #f "echo -n \"~a040000 tree ~a~/~a~%\" | git mktree"
                 (if (string-null? cur) cur (string-append cur "\n")) tid article-name)))))
  (define (detect-status status)
    (match (assoc-ref meta 'status)
      ("publish" "Add")
      ("deleted" "Remove")
      (else "Add")))
  (catch 'colt-err
    (lambda ()
      (let* ((meta-content (gen-meta-content meta))
             (msg (format #f "~a article '~a'" (detect-status meta) article-name))
             (mid (git-hash-object meta-content))
             (cid (git-hash-object content))
             (tid (create-post-dir cid mid)))
        (call-with-values
            (lambda () (create-post-dir cid mid))
          (lambda (tid did)
            (git-update-ref (git-commit-tree did msg #:parent (if init? #f 'HEAD)))
            tid))))
    (lambda (_ proc reason . args)
      (format (current-error-port) "~a: ~a~%"
              proc (apply format #f reason args))
      "failed")))

(define (git-edit-article article-title article-name article-content
                          article-oid article-tags article-status update-timestamp?)
  (let* ((article-meta (git/get-meta-data article-oid)))
    (meta-data-tags-set! article-meta article-tags)
    (meta-data-title-set! article-meta article-title)
    (meta-data-status-set! article-meta article-status)
    (when update-timestamp?
      (meta-data-timestamp-set! article-meta (current-time)))
    (let* ((timestamp (string->number
                       (meta-data-timestamp article-meta)))
           (url-name (uri-encode (gen-proper-url-name
                                  timestamp article-title))))
      (git-post-article url-name article-content (record-type->list article-meta)))))

(define (git-remove-article article-title article-content article-oid)
  (let* ((article-meta (git/get-meta-data article-oid)))
    (meta-data-status-set! article-meta "deleted")
    (let* ((timestamp (string->number
                       (meta-data-timestamp article-meta)))
           (url-name (uri-encode (gen-proper-url-name
                                  timestamp article-title))))
      (git-post-article url-name article-content (record-type->list article-meta)))))

(define (get-all-comment-oids coid)
  (catch 'colt-err
    (lambda ()
      (fold (lambda (x p)
              (match x
                (() p)
                (("tree" _) p)
                ((oid) (cons oid p))
                (else (throw 'colt-err 500 get-all-comment-oids
                             "Invalid comments tree ~a" x))))
            '()
            (cmd-result-contents (cmd git show ,coid))))
    ;; If any error, then there's no comments (just assuming)
    ;; FIXME: Do it more elegantly.
    (lambda e '())))

(define (git/get-content oid)
  (let ((cid (format #f "~a:content" oid)))
    (raw-cmd git show ,cid)))

(define (git/get-comments oid)
  (let* ((coid (format #f "~a:comments" oid))
         (comment-oids (get-all-comment-oids coid)))
    (map new-comment comment-oids)))

(define-syntax-rule (tags-string->list l)
  (map string-trim-both (irregex-split "," l)))

(define (git/get-meta-data oid)
  (define (parse-meta-data moid)
    (call-with-input-string
        (raw-cmd git show ,moid)
      (lambda (port)
        (let* ((timestamp (get-meta-data-value 'timestamp port))
               (tags (get-meta-data-value 'tags port))
               (status (get-meta-data-value 'status port))
               (title (get-meta-data-value 'title port))
               (name (get-meta-data-value 'name port))
               (comment-status (get-meta-data-value 'comment_status port)))
          (make-meta-data timestamp
                          tags
                          status
                          title
                          name
                          comment-status)))))
  (let ((moid (format #f "~a:metadata" oid)))
    (parse-meta-data moid)))

(define (get-post gobj)
  (let* ((oid (git-object-oid gobj))
         (url-name (git-object-file gobj))
         (content (git/get-content oid))
         (comments ""
                   #;
                   (catch 'colt-err              ;
                   (lambda () (git/get-comments oid)) ;
                   (const '())))
         (meta-data (git/get-meta-data oid)))
    (make-post meta-data content comments url-name oid)))

(define (git/get-post-objs)
  (let ((ol (git-ls-tree)))
    (filter
     (lambda (o)
       (not (string=? (meta-data-status (post-meta-data (get-post o)))
                      "deleted")))
     ol)))

(define (git-db-init)
  (cmd git init --bare --quiet)
  (git-update-ref
   (git-commit-tree
    (one-line-result "echo -n \"\" |  git mktree -z")
    "First commit" #:parent #f)))

(define (post-welcome-page)
  (let ((timestamp (current-time)))
    (git-post-article (uri-encode "Welcome to Colt Blog Engine!")
                      "Colt blog engine is written with GNU Artanis, and licenced under GPLv3.
     This project is created in hope of helping people with free software.
     Happy hacking!"
                      `(("timestamp" . ,timestamp)
                        ("tags" . "")
                        ("status" . "publish")
                        ("title"  . "Welcome to Colt Blog Engine!")
                        ("name"  . "Colt")
                        ("comment_status" . "closed"))
                      #:init? #t)))

(define (post-intro-page)
  (let ((timestamp (current-time)))
    (git-post-article (uri-encode "About me")
                      "I'm too lazy to introduce myself at present."
                      `(("timestamp" . ,timestamp)
                        ("tags" . "")
                        ("status" . "publish")
                        ("title"  . "_____colt_____Intro")
                        ("name"  . "Colt")
                        ("comment_status" . "closed")))))

(define (ensure-blog-repo)
  (let ((blog-repo (current-blog-repo)))
    (cmd mkdir -p ,blog-repo)
    (chdir blog-repo)
    (cmd git init --bare --quiet)
    ;;(git-db-init)
    (cmd git config --local user.name "'" ,(colt-conf-get 'blog-author) "'")
    (cmd git config --local user.email "'" ,(colt-conf-get 'blog-author-email) "'")
    (post-welcome-page)
    (post-intro-page)))

(define (enter-blog-repo)
  (chdir (current-blog-repo)))
