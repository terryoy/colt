Colt Blog Engine
=================

[![GPLv3](pub/img/gplv3.png)](https://www.gnu.org/copyleft/gpl.html)

<img src="pub/img/favicon.png" width="50" height="50" />

Colt Blog Engine (CBE) is powered by GNU Artanis which is written in GNU Guile.

# What's special?
* Safe, free from SQL-injection, based on Git database.
* WYSIWYG editor based on Quill.
* Out-of-the-box, download and just run.
* External commenting-system integrated (Disqus, and more in the future).

# Installation

# Install from Docker (Recommended)

First you need to install and configure Docker correctly, see [Docker Installation](https://docs.docker.com/install/).

```bash
mkdir myblog
docker run -it --rm -v $PWD/myblog:/colt/prv/ -p 3000:3000 registry.gitlab.com/nalaginrut/colt bash -c 'cd colt; art work --refresh'
```

Now please visit http://localhost:3000

The default dashboard username is *admin*, and the default password is *123*.
Please don't forget to modify it in *dashboard/settings*.

All your blogs and custimized settings are stored in *myblog* directory in the current directory.
It is strongly recommended store your *myblog/blog* to GitLab or GitHub by `git push` occasionally.

# Install from source code (Expert only)

You have to fetch and compile GNU Artanis:
[Install GNU Artanis](https://www.gnu.org/software/artanis/manual/manual.html#org2eb6710).

# Run

## Run with source code

```bash
git clone https://gitlab.com/NalaGinrut/colt.git my_blog
cd my_blog
art work
```

## Refresh caches

If you updated any code of Colt, or you've upgraded GNU Artanis, please make sure to refresh the caches:

```bash
art work --refresh
```

# Configure

## Blog configure

Please see `prv/colt.scm`.

## Admin password

Please see `prv/passwd.scm`.

# Commenting system

Colt use [Disqus](https://disqus.com/) for its current commenting system. We may add others in the futures.

## Configure Disqus

Please make sure you have Disqus account, and fill your Disqus account name in *dashboard/settings*.

Enjoy!


# Development

## Setup with Artanis Docker image

First you need to checkout colt's source code, then create a folder for a blog folder(e.g. `colt-blog`), then you can run an Artanis docker image to setup all this.

```bash
$ git clone https://gitlab.com/NalaGinrut/colt.git
$ mkdir colt-blog
$ docker run -it -v $PWD/colt:/colt -v $PWD/colt-blog:/colt/prv/ -p 3000:3000 registry.gitlab.com/nalaginrut/artanis bash -c 'cd colt; art work --refresh'
```

