/*
  @licstart  The following is the entire license notice for the
  JavaScript code in this page.

  Copyright (C) 2019  Mu Lei known as NalaGinrut <mulei@gnu.org>

  The JavaScript code in this page is free software: you can
  redistribute it and/or modify it under the terms of the GNU
  General Public License (GNU GPL) as published by the Free Software
  Foundation, either version 3 of the License, or (at your option)
  any later version.  The code is distributed WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

  As additional permission under GNU GPL version 3 section 7, you
  may distribute non-source (e.g., minimized or compacted) forms of
  that code without the copy of the GNU GPL normally required by
  section 4, provided you include this license notice and a URL
  through which recipients can access the Corresponding Source.


  @licend  The above is the entire license notice
  for the JavaScript code in this page.
*/
var children_nodes;
var quill;
var url_name;
var article_oid;

function new_post(obj) {
    let article_title = document.getElementById('edit-article-title');
    hljs.configure({
        languages: ['javascript', 'ruby', 'python', 'lua', 'erlang',
                    'c++', 'scheme', 'rust', 'haskell', 'bash']
    });

    quill = new Quill('#editor-container', {
        modules: {
            syntax: true,
            toolbar: '#toolbar-container',
            imageResize: {
                modules: ['Resize', 'DisplaySize', 'Toolbar'],
                handleStyles: {
                    backgroundColor: 'black'
                }
            }
        },
        theme: 'snow'
    });

    quill.root.innerHTML = localStorage.getItem('colt.saved_content');
    setInterval(_=>{ localStorage.setItem('colt.saved_content', quill.root.innerHTML); }, 1000);
}

function edit_post(obj) {
    children_nodes = obj.parentNode.parentNode.children;
    //console.log(obj);
    //console.log(children_nodes);
    //console.log(children_nodes[1].firstChild);
    let article_title = document.getElementById('edit-article-title');
    let article_content = document.getElementById('articles-content');
    let article_tags = document.getElementById('edit-article-tags');
    let article_status = document.getElementById('edit-article-status');
    let edit_content = document.getElementById('edit-content');
    let editor_container = document.getElementById("editor-container");
    article_content.style.display = "none";
    edit_content.style.display = "block";

    if (article_title !== null && typeof article_title !== 'undefined')
    {
        article_title.value = children_nodes[0].firstChild.innerHTML;
        article_tags.value = [...children_nodes[2].children].map(a=>a.innerText).join(', ');
        article_status.value = children_nodes[3].innerText;
    }
    article_oid = children_nodes[6].innerHTML;
    url = children_nodes[0].firstChild.href;
    url_name = url.substring(url.lastIndexOf('/') + 1);
    editor_container.innerHTML = children_nodes[5].innerHTML;

    hljs.configure({
        languages: ['javascript', 'ruby', 'python', 'lua', 'erlang',
                    'c++', 'scheme', 'rust', 'haskell', 'bash']
    });

    quill = new Quill('#editor-container', {
        modules: {
            syntax: true,
            toolbar: '#toolbar-container',
            imageResize: {
                modules: ['Resize', 'DisplaySize', 'Toolbar'],
                handleStyles: {
                    backgroundColor: 'black'
                }
            }
        },
        theme: 'snow'
    });
}

function cancel_edit() {
    location.reload();
}

function show_msg_layer(msg) {
    let ml = document.getElementById('msg-layer');
    ml.innerHTML = msg;
    ml.classList.add('msg-layer');
    ml.addEventListener('click', function (e) {
        console.log(e);
        ml.classList.remove('msg-layer');
    });
}

function intro_submit() {
    let article_content = quill.root.innerHTML;
    let title = "_____colt_____Intro";
    let tags = "";
    let status = "publish";

    // Quill will embed redundnat <br> which breaks XML validation, we fix it here.
    article_content.replace("<p><br></p>", "");

    let submit_button = document.getElementById("submit-button");
    submit_button.innerText = "Submiting...";
    submit_button.disabled = true;

    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "/v1/colt/edit_article",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                "title": title,
                "name": "About me",
                "content": article_content,
                "status": status,
                "oid": article_oid,
                "tags": tags,
                "update_timestamp": false
            }),
            success: function (oid) {
                if ("failed" === oid)
                {
                    show_msg_layer("Edit failed, please check!");
                    submit_button.innerText = "Submit";
                    submit_button.disabled = false;
                }
                else
                    window.location.pathname = "/about";

                localStorage.setItem('colt.saved_content', "");
            }
        })
    })
}

function edit_submit() {
    let article_content = quill.root.innerHTML;
    let title = document.getElementById('edit-article-title').value;
    let tags = document.getElementById('edit-article-tags').value;
    let status = document.getElementById('edit-article-status').value
    let update_timestamp = document.getElementById('edit-update-timestamp');

    let submit_button = document.getElementById("submit-button");
    submit_button.innerText = "Submiting...";
    submit_button.disabled = true;

    // Quill will embed redundnat <br> which breaks XML validation, we fix it here.
    article_content.replace("<p><br></p>", "");


    fetch('/v1/colt/edit_article', {
        method: 'POST',
        body: JSON.stringify({
            "title": title,
            "name": url_name,
            "content": article_content,
            "status": status,
            "oid": article_oid,
            "tags": tags,
            "update_timestamp": update_timestamp.checked
        }),
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }})
        .then(response => response.text())
        .then(oid => {
            if ("failed" === oid)
            {
                show_msg_layer("Edit failed, please check!");
                submit_button.innerText = "Submit";
                submit_button.disabled = false;
            }
            else if (typeof title == 'undefined')
                window.location.pathname = "/about";
            else
                window.location.pathname = "/article/preview/oid/" + oid;

            localStorage.setItem('colt.saved_content', "");
        })
        .catch(error => alert(error));
}

function submit_post() {
    let submit_button = $("#submit-button")[0];
    submit_button.innerText = "Submiting...";
    submit_button.disabled = true;

    let article_content = quill.root.innerHTML;
    let title = $('#article-title')[0].value;
    let author = $('#article-author')[0].value;
    let tags = $('#article-tag')[0].value;
    let status = $('#publish-status')[0].value;

    /* Remove redundant BOM */
    article_content = article_content.replace(/[\u200B-\u200D\uFEFF]/g, '');
    // Quill will embed redundnat <br> which breaks XML validation, we fix it here.
    article_content.replace("<p><br></p>", "");

    fetch('/v1/colt/post_article', {
        method: 'POST',
        body: JSON.stringify({
            "name": url_name,
            "author": author,
            "title": title,
            "tags": tags,
            "status": status,
            "content": article_content,
        }),
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }})
        .then(response => response.text())
        .then(oid => {
            alert(oid);
            if ("failed" === oid)
            {
                show_msg_layer("Post new article failed, please check!");
                return;
            }

            if ("_____colt_____Intro" === title)
                window.location.pathname = "/about";
            else
                window.location.pathname = "/article/preview/oid/" + oid;

            // We shouldn't clean the saved content before it submitted successfully
            localStorage.setItem('colt.saved_content', "");
        })
        .catch(error => alert(error));
}

var mmr;

function setme(obj){
    mmr=obj;
}

function delete_post(obj) {
    let children_nodes = obj.parentNode.parentNode.children;
    setme(children_nodes);

    let article_content = children_nodes[5].innerHTML;
    let article_oid = children_nodes[6].innerText;
    let article_title = children_nodes[0].innerText;

    let delete_button = document.getElementById("delete-button-" + article_oid);
    delete_button.innerText = "Deleting...";
    delete_button.disabled = true;

    fetch('/v1/colt/remove_article', {
        method: 'POST',
        body: JSON.stringify({
            "title": article_title,
            "content": article_content,
            "oid": article_oid
        }),
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }})
        .then(response => response.text())
        .then(status => {
            if ("ok" === status)
                location.reload();
            else
                show_msg_layer("Delete failed, please check!");

            // We shouldn't clean the saved content before it submitted successfully
            localStorage.setItem('colt.saved_content', "");
        })
        .catch(error => alert(error));
}

function init_intro_page() {
    let ac = document.getElementById('articles-content');
    document.getElementsByClassName('edit-buttons')[0].click();
    ac.visibility = 'visible';
}
